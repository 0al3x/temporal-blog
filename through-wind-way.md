# Through wind way
> 31/03/2024 00:16


Entropía, mi primer acercamiento a este concepto fue durante el penúltimo de los semestres a los que
asistí en la universidad. Desde entonces hasta el día de hoy mi precepto y comprensión de dicho 
concepto ha ido enriqueciéndose con el paso del tiempo. 

Partiendo con mi primera comprensión en el ámbito estadístico, hasta hoy, donde entiendo en mi 
limitado conocimiento, es su significado más puro y general en el ámbito de la física.

Es pues, el paralelismo a este concepto, la analogía más fiel y precisa que se me ocurre, y de la 
que dispongo como herramienta para describir este momento. 

Es así pues, desde el punto de vista estadístico un momento de entropía máxima. Un momento, el cual 
se desvela como uno en el que nunca antes el camino había estado tan oscuro. 

La incertidumbre, palabra que de forma perfecta puede servir como adjetivo descriptivo, de este, mi 
actual presente, es máxima.

O bueno, rectifico, casi máxima. Pues, alguna recóndita parte de mí, muy en el fondo, está segura de
que no moriré en el corto plazo.

Pero tan solo estadísticamente este momento es uno de entropía máxima. Desde el punto de vista de la 
física hace ya mucho tiempo que el nivel de entropía de mi cuerpo desciende de forma constante hacia 
la disipación. 

Tanto así que, hoy con suerte, puedo concentrar la energía necesaria para concretar la tarea de 
plasmar el momento, este momento, aquel momento de la noche anterior, a desalojar mi actual refugio 
de pernoctación.

Buenas noches.
